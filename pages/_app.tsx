import '../styles/globals.scss';
import type {AppProps} from 'next/app';
import * as Sentry from '@sentry/nextjs';
import React from 'react';
import Head from 'next/head';

function MyApp({Component, pageProps}: AppProps) {
  Sentry.init({
    dsn: 'https://a29a5712b72748cebed1b03f25133408@o866210.ingest.sentry.io/6049204',

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 0.5,
  });

  return (
    <>
      <Head>
        <meta name="viewport" content="width=1920" />
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
