import {NextPage} from 'next';
import React from 'react';

const NotFoundError: NextPage = () => {
  return (
    <div className="d-flex align-items-center justify-content-center h-100 w-100">
      <h3>Запрашиваемая приставка не найдена или отключена :(</h3>
    </div>
  );
};
export default NotFoundError;
