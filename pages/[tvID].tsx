import type {GetServerSidePropsContext, NextPage} from 'next';
import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {TvContext} from '../components/context';
import ErrorPage from '../components/ErrorPage';
import Header from '../components/Header.component';

import MainLayout from '../components/MainLayout';
import WaterTemperatureSection from '../components/sections/WaterTemperatureSection.component';

import WeatherMinSectioncomponent from '../components/sections/WeatherMinSection.component';
import WeatherSection from '../components/sections/WeatherSection.component';
import BackendService from '../helpers/BackendService';
import {getModuleRotationTime, modulesList} from '../helpers/helpers';
import {TvEntity, TvModulesType} from '../helpers/types/TvTypes';
import ReactFullpage from '@fullpage/react-fullpage';
import {ReactFullPageApi} from '../helpers/types/CommonTypes';

import Loader from '../components/Loader.component';
import {BeachPoint} from '../helpers/types/BeachPoint.interface';
import {AxiosError} from 'axios';
import AdvertiseSectionComponent from '../components/sections/AdvertiseSection.component';

const Home: NextPage<{
  tv: TvEntity;
  isError?: boolean;
  beachData?: BeachPoint;
}> = ({tv, beachData, isError}) => {
  const [loading, setLoading] = useState<boolean>(true);
  const fullpageApi = React.createRef<ReactFullpage & ReactFullPageApi>();

  const enabledModules = useMemo(() => {
    const modulesEnabled: TvModulesType[] = [];
    if (tv)
      modulesList.map((moduleName) => {
        if (
          tv.modules_enabled?.find(
            (module) => module.module_code === moduleName,
          )
        )
          modulesEnabled.push(moduleName);
      });
    return modulesEnabled;
  }, [tv]);

  const [activeModule, setActiveModule] = useState<TvModulesType>(
    enabledModules[0],
  );

  const [slideChangeIsEnabled, setSlideChangeIsEnabled] =
    useState<boolean>(true);

  const changeAciveModule = useCallback(() => {
    // If current active module is the last active module - move to the first module
    if (activeModule === enabledModules.slice(-1).pop())
      fullpageApi.current?.fullpageApi.moveTo(1, 1);
    // Otherwise, just move to the next module
    else fullpageApi.current?.fullpageApi.moveSectionDown();
  }, [activeModule, enabledModules, fullpageApi]);

  // Switch between modules screens with different intervals
  useEffect(() => {
    let timeout: NodeJS.Timeout;
    if (tv && fullpageApi && activeModule && fullpageApi.current) {
      if (loading) setLoading(false);

      if (slideChangeIsEnabled) {
        const interval = getModuleRotationTime(tv, activeModule, 5);

        timeout = setTimeout(() => {
          changeAciveModule();
        }, interval);
      }
    }
    return () => clearTimeout(timeout);
  }, [
    tv,
    fullpageApi,
    activeModule,
    loading,
    enabledModules,
    slideChangeIsEnabled,
    changeAciveModule,
  ]);

  const onSlideChange = useCallback(
    (
      origin: any,
      destination: {
        index: number;
        isFirst: boolean;
        isLast: boolean;
        item: HTMLElement;
      },
    ) => {
      setActiveModule(enabledModules[destination.index]);
    },
    [enabledModules],
  );

  useEffect(() => {
    console.log('Slide changing is enabled - ', slideChangeIsEnabled);
  }, [slideChangeIsEnabled]);

  const toggleSlideChanging = (state: boolean): void => {
    setSlideChangeIsEnabled(state);
  };

  return isError ? (
    <ErrorPage />
  ) : (
    <MainLayout title={tv.title}>
      {loading ? <Loader /> : null}
      <Header
        activeModule={activeModule}
        tv={tv}
        minimal={activeModule === 'advertisement'}
      />
      <ReactFullpage
        easing="easeInOutCubic"
        easingcss3="ease"
        css3={true}
        ref={fullpageApi}
        onLeave={onSlideChange}
        scrollingSpeed={900} /* Options here */
        render={() => {
          return (
            <TvContext.Provider
              value={{
                tv,
                beachData,
                activeModule,
                toggleSlideChanging,
                changeAciveModule,
                slideChangeIsEnabled,
              }}
            >
              <ReactFullpage.Wrapper>
                <div className="modules">
                  {enabledModules.map((module) => {
                    return (
                      <div className="section" key={module}>
                        {module === 'weather_min' ? (
                          <WeatherMinSectioncomponent />
                        ) : module === 'weather' ? (
                          <WeatherSection />
                        ) : module === 'water_temperature' ? (
                          <WaterTemperatureSection />
                        ) : module === 'advertisement' ? (
                          <AdvertiseSectionComponent />
                        ) : null}
                      </div>
                    );
                  })}
                </div>
              </ReactFullpage.Wrapper>
            </TvContext.Provider>
          );
        }}
      />
    </MainLayout>
  );
};

export async function getServerSideProps(
  context: GetServerSidePropsContext,
): Promise<
  | {props: {tv: TvEntity; beachData?: BeachPoint}}
  | {props: {isError: boolean}}
  | {notFound: boolean}
> {
  const {tvID} = context.query;

  if (!tvID)
    return {
      notFound: true,
    };
  const backendSerive = new BackendService();

  try {
    const tv = await backendSerive.getTvById(tvID as string);

    if (!tv || !tv.enabled)
      return {
        notFound: true,
      };

    // Get beach entity for a current Tv
    let beachData;
    if (tv.beach_id) beachData = await backendSerive.getBeachData(tv.beach_id);

    return {
      props: {
        tv: tv,
        beachData,
      },
    };
  } catch (error) {
    if (error && (error as AxiosError).response?.status === 404)
      return {
        notFound: true,
      };

    backendSerive.handleError(error);
    return {
      props: {
        isError: true,
      },
    };
  }
}

export default Home;
