import React from 'react';

const ErrorPage = () => {
  return (
    <div
      className="error-page d-flex align-items-center justify-content-center w-100"
      style={{height: '100vh'}}
    >
      <h3 className="d-inline-block">Упс! Что-то пошло не так....</h3>
    </div>
  );
};
export default ErrorPage;
