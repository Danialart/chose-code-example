import {FunctionComponent, useMemo} from 'react';
import {Notice} from '../../../helpers/types/NoticesTypes';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import moment from 'moment-timezone';
import styles from '../../../styles/Modules/Chart.module.scss';

const WaterTemperatureChart: FunctionComponent<{data: Notice[]}> = ({data}) => {
  // Get only data that have at least 4 hours of difference between each other. And also memorize this vaue.
  const dataFormatted = useMemo(() => {
    let currentItem: Notice | undefined;

    return data
      .filter((item, index) => {
        if (!currentItem) {
          currentItem = item;
          return true;
        } else {
          const previousTime = moment.unix(currentItem.created);
          const currentTime = moment.unix(item.created);

          if (
            currentTime.diff(previousTime, 'hours') >= 4 ||
            index === data.length - 1
          ) {
            currentItem = item;
            return true;
          }
        }
      })
      .map((item) => ({x: item.created, y: item.termo}));
  }, [data]);

  const maxDataValue: number = useMemo(
    () => Math.max(...data.map((notice) => notice.termo)),
    [data],
  );

  return (
    <HighchartsReact
      highcharts={Highcharts}
      containerProps={{
        className: styles.chartWrapper,
      }}
      options={
        {
          credits: {
            enabled: false,
          },

          title: {
            text: undefined,
          },

          mapNavigation: {
            enabled: true,
            enableButtons: false,
          },
          chart: {
            marginTop: 30,
            spacingBottom: 38,
            spacingLeft: 20,
            spacingRight: 30,
          },
          xAxis: {
            type: 'datetime',

            lineWidth: 0,
            tickLength: 0,
            labels: {
              style: {
                color: '#888',
                fontSize: '16px',
                fontFamily: 'Open Sans',
              },
              formatter: function () {
                return moment.unix(this.value as number).format('HH:mm'); // clean, unformatted number for year
              },
            },
          },
          plotOptions: {
            area: {
              fillColor: {
                linearGradient: {
                  x1: 0,
                  y1: 0,
                  x2: 0,
                  y2: 1,
                },
                stops: [
                  [0, 'rgba(35, 153, 248, 1)'],
                  [1, 'rgba(255, 255, 255, 0)'],
                ],
              },
            },
          },
          yAxis: {
            gridLineDashStyle: 'LongDash',
            tickInterval: 3,
            gridLineColor: '#EDF2F0',
            title: {
              text: '',
            },
            labels: {
              style: {
                color: '#888',
                fontSize: '17px',
                fontFamily: 'Open Sans',
              },
            },
            min: 0,
            max: +(maxDataValue * 0.3 + maxDataValue).toFixed(0),
          },
          series: [
            {
              name: 'День',
              type: 'area',
              showInLegend: false,
              dashStyle: 'Solid',
              lineWidth: 3,
              lineColor: '#2399F8',
              marker: {
                enabled: true,
                symbol: 'circle',
                lineColor: '#2399F8',
                fillColor: '#fff',
                lineWidth: 3,
                radius: 6,
              },
              data: dataFormatted,
              turboThreshold: 50000,
            },
          ],
        } as Highcharts.Options
      }
    />
  );
};

export default WaterTemperatureChart;
