import classNames from 'classnames';
import React, {FunctionComponent, useMemo} from 'react';
import {RoundValue} from '../../../helpers/helpers';
import styles from '../../../styles/Modules/WaterComfortScale.module.scss';

const WaterComfortScale: FunctionComponent<{waterTemperature: number}> = ({
  waterTemperature,
}) => {
  const minimalValue = 16;
  const maximalValue = 30;
  const availableScale = 14;

  const currentPercent = useMemo(
    () =>
      waterTemperature < minimalValue
        ? 3.5
        : waterTemperature >= maximalValue
        ? 97
        : ((waterTemperature - minimalValue) / availableScale) * 100,
    [waterTemperature],
  );

  const bgColor: string =
    currentPercent < 14.5
      ? 'rgba(42, 154, 245, 1)'
      : currentPercent < 30.5
      ? '#CCB1C8'
      : currentPercent < 51.5
      ? '#FBB7BB'
      : currentPercent < 85.4
      ? 'rgba(255, 148, 69, 1)'
      : 'rgba(247, 89, 11, 1)';

  return (
    <div className={styles.WaterComfortScale}>
      <div className={styles.scaleWrapper}>
        <div
          className={styles.pointer}
          style={{left: `${currentPercent}%`, backgroundColor: bgColor}}
        >
          {RoundValue(waterTemperature, 1)}˚
        </div>
        <div className={styles.comfortArea}></div>
        <div className={styles.scaleLine}></div>
      </div>
      <div
        className={classNames(
          'd-flex justify-content-between',
          styles.description,
        )}
      >
        <div className={styles.cold}>холодно</div>
        <div className={styles.comfort}>комфортно</div>
        <div className={styles.warm}>горячо</div>
      </div>
    </div>
  );
};

export default WaterComfortScale;
