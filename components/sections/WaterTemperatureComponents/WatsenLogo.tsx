import Image from 'next/image';

const WatsenLogo = () => {
  return (
    <Image
      src="/client/tv/watsen-logo.png"
      alt="Watsen"
      width="329"
      height="94"
    />
  );
};

export default WatsenLogo;
