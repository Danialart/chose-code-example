import classNames from 'classnames';
import React, {FunctionComponent, useContext, useEffect, useState} from 'react';
import BackendService from '../../helpers/BackendService';
import {
  calculateWindDirection,
  getStormData,
  RoundValue,
} from '../../helpers/helpers';
import {WeatherMin} from '../../helpers/types/WeatherType';

import styles from '../../styles/Modules/WeatherMin.module.scss';
import {TvContext} from '../context';
import Loader from '../Loader.component';

const WeatherMinSectioncomponent: FunctionComponent = () => {
  const [weatherData, setWeatherData] = useState<WeatherMin | undefined | null>(
    undefined,
  );

  const {tv, beachData} = useContext(TvContext);

  useEffect(() => {
    const backendService = new BackendService();
    if (tv.meteo_station_id) {
      backendService
        .getWeatherMinData(tv.meteo_station_id, 1)
        .then((res) => setWeatherData(res))
        .catch((error) => console.error(error));

      const interval = setInterval(() => {
        if (tv.meteo_station_id)
          backendService
            .getWeatherMinData(tv.meteo_station_id, 1)
            .then((res) => setWeatherData(res))
            .catch((error) => console.error(error));
      }, 60000);

      return () => clearInterval(interval);
    } else setWeatherData(null);
  }, [tv]);

  return (
    <div className={styles.weatherMinWrapper}>
      {weatherData ? (
        <>
          <div
            className={classNames(styles.itemsRow, 'd-flex')}
            style={{marginBottom: 104}}
          >
            <div className={styles.item}>
              <div className={styles.blockName}>Воздух</div>
              <div className={styles.value}>
                {RoundValue(weatherData.t, 1)} <small>˚C</small>
              </div>
            </div>
            <div className={styles.item}>
              <div className={styles.blockName}>Давление</div>
              <div className={styles.value}>
                {RoundValue(weatherData.PR, 0)} <small>рт.с</small>
              </div>
            </div>
            <div className={styles.item}>
              <div className={styles.blockName}>Влажность</div>
              <div className={styles.value}>
                {RoundValue(weatherData.HM, 0)}
                <small>%</small>
              </div>
            </div>
            {beachData?.lastNotice ? (
              <div className={styles.item}>
                <div className={styles.blockName}>Вода</div>
                <div className={styles.value}>
                  {RoundValue(beachData.lastNotice.termo, 1)}
                  <small>˚C</small>
                </div>
              </div>
            ) : null}
          </div>
          <div className={classNames(styles.itemsRow, 'd-flex')}>
            <div className={styles.item}>
              <div className={styles.blockName}>Ветер</div>
              <div className={styles.value}>
                {~weatherData.WV
                  ? RoundValue(weatherData.WV, 1)
                  : RoundValue(weatherData.WV2)}{' '}
                <small>м/с</small>
              </div>
            </div>
            {beachData?.gradations.length ? (
              <div className={styles.item}>
                <div className={styles.blockName}>Море</div>
                <div className={styles.value}>
                  <small className="text-uppercase">
                    {getStormData(
                      beachData.gradations,
                      weatherData.WV,
                      weatherData.WD,
                    )}
                  </small>
                </div>
              </div>
            ) : null}
            <div className={styles.item}>
              <div className={styles.blockName}>Направление</div>
              <div className={styles.value}>
                {calculateWindDirection(weatherData.WD)}
              </div>
            </div>
            <div className={styles.item}>
              <div className={styles.blockName}>УФ</div>
              <div className={styles.value}>
                {weatherData.UV}
                <small>%</small>
              </div>
            </div>
          </div>
        </>
      ) : weatherData === null ? (
        <h4 className="text-center">
          Не указана метеостанция для загрузки данных
        </h4>
      ) : (
        <Loader className={styles.loader} />
      )}
    </div>
  );
};
export default WeatherMinSectioncomponent;
