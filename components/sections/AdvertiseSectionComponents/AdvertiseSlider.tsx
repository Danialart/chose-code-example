import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import {AdvertiseType} from '../../../helpers/types/AdvertiseTypes';
import styles from '../../../styles/Modules/Advertise.module.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import {TvContext} from '../../context';
import ImageBanner from './ImageBanner';
import AdvertiseVideo from './YoutubeVideo';

const AdvertiseSlider: FunctionComponent<{banners: Array<AdvertiseType>}> = ({
  banners,
}) => {
  const {tv, activeModule, slideChangeIsEnabled} = useContext(TvContext);

  const autoplaySpeed = tv.advertisement_settings?.rotation_time ?? 5;

  const ref = React.createRef<
    Slider & {
      innerSlider: {[x: string]: any};
    }
  >();

  const [activeSlide, setActiveSlide] = useState<number>(0);

  useEffect(() => {
    if (ref.current) {
      if (activeModule !== 'advertisement') {
        ref.current.slickGoTo(0);
        ref.current.slickPause();
      } else {
        ref.current.slickPlay();
      }
    }
  }, [ref, activeModule]);

  return (
    <Slider
      className={styles.bannersSlider}
      dots={false}
      infinite={true}
      speed={1000}
      slidesToScroll={1}
      slidesToShow={1}
      fade={true}
      afterChange={(currentSlide) => setActiveSlide(currentSlide)}
      ref={ref}
      cssEase="linear"
      slidesPerRow={1}
      autoplay={slideChangeIsEnabled}
      autoplaySpeed={autoplaySpeed * 1000}
    >
      {banners.map((bannerItem, index) => (
        <div key={bannerItem.id} className={styles.slide}>
          {bannerItem.type === 'photo' ? (
            <ImageBanner url={bannerItem.url} title={bannerItem.title} />
          ) : (
            <AdvertiseVideo
              isActive={
                activeSlide === index && activeModule === 'advertisement'
              }
              url={bannerItem.url}
            />
          )}
        </div>
      ))}
    </Slider>
  );
};
export default AdvertiseSlider;
