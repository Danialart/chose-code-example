import React, {FunctionComponent, useState} from 'react';
import Image from 'next/image';
import {formatImagePath} from '../../../helpers/helpers';
import Loader from '../../Loader.component';
import styles from '../../../styles/Modules/Advertise.module.scss';

const ImageBanner: FunctionComponent<{url: string; title: string}> = ({
  url,
  title,
}) => {
  const [loading, setLoading] = useState<boolean>(true);

  return (
    <>
      {loading ? <Loader className={styles.imageLoader} /> : null}
      <Image
        onLoadingComplete={() => setLoading(false)}
        loading="lazy"
        src={formatImagePath(url)}
        alt={title}
        layout="fill"
      />
    </>
  );
};

export default ImageBanner;
