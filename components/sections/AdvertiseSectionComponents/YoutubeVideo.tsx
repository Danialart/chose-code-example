import React, {FunctionComponent, useContext, useEffect} from 'react';
import ReactPlayer from 'react-player';
import {TvContext} from '../../context';

const AdvertiseVideo: FunctionComponent<{url: string; isActive: boolean}> = ({
  url,
  isActive,
}) => {
  const playerRef = React.createRef<
    ReactPlayer & {
      player: {
        isPlaying: boolean;
      };
    }
  >();

  console.log('Видео', isActive);

  const {toggleSlideChanging, changeAciveModule} = useContext(TvContext);

  useEffect(() => {
    // Rewinding to the begining (if video is loaded, was played but not active anymore)
    if (
      playerRef.current &&
      isActive === false &&
      playerRef.current.getCurrentTime() !== null &&
      playerRef.current.getCurrentTime() > 0
    ) {
      playerRef.current.seekTo(0);
      toggleSlideChanging(true);
    }
  }, [playerRef, isActive, toggleSlideChanging]);

  return (
    <ReactPlayer
      url={url}
      playing={isActive}
      ref={playerRef}
      width="100%"
      height="100%"
      onProgress={({played}) => {
        if (played > 0) toggleSlideChanging(false);
      }}
      className="react-player"
      controls={false}
      onEnded={() => {
        setTimeout(() => {
          changeAciveModule();
          toggleSlideChanging(true);
        }, 500);
      }}
    />
  );
};
export default AdvertiseVideo;
