import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {TvContext} from '../context';
import styles from '../../styles/Modules/Weather.module.scss';
import BackendService from '../../helpers/BackendService';
import {WeatherType} from '../../helpers/types/WeatherType';
import Loader from '../Loader.component';
import moment from 'moment';

import 'moment/locale/ru';
import classNames from 'classnames';
import WeatherItemComponent from './WeatherSectionComponents/WeatherItem.component';

const WeatherSection: FunctionComponent = () => {
  const {tv} = useContext(TvContext);

  const [weatherData, setWeatherData] = useState<
    WeatherType[] | undefined | null
  >();

  const filterWeather = useCallback((item: WeatherType) => {
    const maximalDate = moment().utc().add(1, 'day').endOf('day');
    const itemDate = moment.utc(item.date);
    const itemDateHours = moment.utc(item.date).format('HH:mm');

    return (
      itemDate <= maximalDate &&
      (itemDateHours === '00:00' ||
        itemDateHours === '06:00' ||
        itemDateHours === '12:00' ||
        itemDateHours === '18:00')
    );
  }, []);

  const sortByDate = useCallback((a: WeatherType, b: WeatherType) => {
    const aDate = moment.utc(a.date);
    const bDate = moment.utc(b.date);

    return bDate < aDate ? 1 : bDate > aDate ? -1 : 0;
  }, []);

  useEffect(() => {
    const backendService = new BackendService();

    if (tv.beach_id) {
      // Get data for the first load
      backendService
        .getWeatherData(tv.beach_id)
        .then((res) => {
          setWeatherData(res.filter(filterWeather).sort(sortByDate));
        })
        .catch((error) => {
          console.error(error);
          setWeatherData(null);
        });

      // Get actual data after each minute
      const interval = setInterval(() => {
        if (tv.beach_id)
          backendService
            .getWeatherData(tv.beach_id)
            .then((res) =>
              setWeatherData(res.filter(filterWeather).sort(sortByDate)),
            )
            .catch((error) => {
              console.error(error);
            });
      }, 60000);

      return () => clearInterval(interval);
    } else setWeatherData(null);
  }, [filterWeather, sortByDate, tv]);

  // Memoize slices of weather data
  const {
    todayWeather,
    tomorrowWeather,
  }: {
    todayWeather: JSX.Element[] | null;
    tomorrowWeather: JSX.Element[] | null;
  } = useMemo(() => {
    if (weatherData)
      return {
        todayWeather: weatherData
          .slice(0, 4)
          .map((weatherItem) => (
            <WeatherItemComponent item={weatherItem} key={weatherItem.id} />
          )),
        tomorrowWeather: weatherData
          .slice(4, weatherData.length)
          .map((weatherItem) => (
            <WeatherItemComponent item={weatherItem} key={weatherItem.id} />
          )),
      };
    else
      return {
        todayWeather: null,
        tomorrowWeather: null,
      };
  }, [weatherData]);

  return (
    <div className={styles.WeatherModuleWrapper}>
      {weatherData ? (
        <>
          <div className={styles.column}>
            <div className={styles.title}>
              Сегодня {moment().locale('ru').format('DD MMMM')}
            </div>
            <div className={classNames('d-flex', styles.wrapper)}>
              {todayWeather}
            </div>
          </div>
          <div className={styles.column}>
            <div className={styles.title}>
              Завтра{' '}
              {moment()
                .startOf('day')
                .add(1, 'day')
                .locale('ru')
                .format('DD MMMM')}
            </div>
            <div className={classNames('d-flex', styles.wrapper)}>
              {tomorrowWeather}
            </div>
          </div>
        </>
      ) : weatherData !== null ? (
        <Loader className={styles.loader} />
      ) : (
        <h3>Нет данных</h3>
      )}
    </div>
  );
};

export default WeatherSection;
