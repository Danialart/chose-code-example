import {useContext} from 'react';
import {TvContext} from '../context';
import styles from '../../styles/Modules/Advertise.module.scss';
import AdvertiseSlider from './AdvertiseSectionComponents/AdvertiseSlider';

const AdvertiseSectionComponent = () => {
  const {tv} = useContext(TvContext);

  const banners = tv.advertisements;

  return (
    <div className={styles.AdvertiseWrapper}>
      {!banners?.length ? (
        <div className="h-100 w-100 d-flex align-items-center justify-content-center fs-3">
          Нет баннеров для отображения :(
        </div>
      ) : (
        <AdvertiseSlider banners={banners} />
      )}
    </div>
  );
};
export default AdvertiseSectionComponent;
