import React, {FunctionComponent, useMemo} from 'react';
import {WeatherType} from '../../../helpers/types/WeatherType';

import styles from '../../../styles/Modules/Weather.module.scss';
import moment from 'moment';
import classNames from 'classnames';
import {RoundValue} from '../../../helpers/helpers';
import {getWeatherIcon} from '../../../helpers/WeatherIcons.helper';
import Image from 'next/image';

const WeatherItemComponent: FunctionComponent<{item: WeatherType}> = ({
  item,
}) => {
  const currentItemDate = useMemo(() => moment.utc(item.date), [item.date]);
  const dateFormatted = useMemo(
    () => currentItemDate.format('HH:mm'),
    [currentItemDate],
  );

  const itemName = useMemo(() => {
    if (dateFormatted == '00:00') return 'ночь';
    if (dateFormatted == '06:00') return 'утро';
    if (dateFormatted == '12:00') return 'день';
    if (dateFormatted == '18:00') return 'вечер';
  }, [dateFormatted]);

  const weatherIcon = useMemo(() => getWeatherIcon(item.weather_icon), [item]);

  const getWindPower = useMemo(() => {
    const speed = item.wind_speed;
    return speed > 4 && speed < 10
      ? 'Средний'
      : speed > 10
      ? 'Сильный'
      : 'Легкий';
  }, [item]);

  return (
    <div className={styles.timeframeColumn}>
      <div className={styles.itemTitle}>
        <span className={classNames('text-capitalize', styles.descr)}>
          {itemName}
        </span>{' '}
        <span className={styles.time}>{dateFormatted}</span>
      </div>
      <div
        className={classNames('d-flex justify-content-between', styles.data)}
      >
        <div className={styles.info}>
          <div className={styles.infoItem}>
            <small>Воздух</small>
            {RoundValue(item.temp, 1)}
            <span className={styles.pointer}> ˚C</span>
          </div>
          <div className={styles.infoItem}>
            <small>Ветер</small>
            {getWindPower}
          </div>
        </div>
        <div className={styles.icon}>
          {weatherIcon ? (
            <Image
              quality="85"
              layout="intrinsic"
              width="113"
              height="113"
              objectFit="contain"
              alt={weatherIcon}
              src={weatherIcon as string}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default WeatherItemComponent;
