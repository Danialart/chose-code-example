import React, {
  FunctionComponent,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {TvContext} from '../context';
import styles from '../../styles/Modules/WaterTemp.module.scss';
import Loader from '../Loader.component';
import classNames from 'classnames';
import BackendService from '../../helpers/BackendService';
import {Notice} from '../../helpers/types/NoticesTypes';

import WaterTemperatureChart from './WaterTemperatureComponents/WaterTemperatureChart';
import {RoundValue} from '../../helpers/helpers';
import moment from 'moment';
import plural from 'plural-ru';
import WaterComfortScale from './WaterTemperatureComponents/WaterComfortScale';
import WatsenLogo from './WaterTemperatureComponents/WatsenLogo';

const WaterTemperatureSection: FunctionComponent = () => {
  const {beachData} = useContext(TvContext);

  const [loading, setLoading] = useState(true);

  const [notices, setNotices] = useState<Notice[]>();

  useEffect(() => {
    const backendService = new BackendService();

    const getNoticesAndSetData = (sensorId: number) => {
      backendService
        .getSensorData(sensorId)
        .then((res) => {
          setNotices(res);
          setLoading(false);
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
        });
    };

    let interval: NodeJS.Timer;
    if (beachData && beachData.sensor_number) {
      getNoticesAndSetData(beachData.sensor_number);

      interval = setInterval(
        () => getNoticesAndSetData(beachData.sensor_number),
        60000,
      );
    } else if (loading) setLoading(false);

    return () => clearInterval(interval);

    // Get
  }, [beachData, loading]);

  const waterTempUpdateTime = useMemo(
    () =>
      notices?.length &&
      moment().diff(
        moment.unix(notices.slice(-1).pop()?.created as number),
        'minutes',
      ) > 0
        ? plural(
            moment().diff(
              moment.unix(notices.slice(-1).pop()?.created as number),
              'minutes',
            ),
            '%d минута',
            '%d минуты',
            '%d минут',
          )
        : 0,
    [notices],
  );

  return (
    <div className={styles.WaterTemperature}>
      {!loading && notices && notices ? (
        <>
          <div className={classNames(styles.column, styles.tempGraphColumn)}>
            <div className={styles.title}>Данные за 24 часа</div>
            <div className={styles.chart}>
              <WaterTemperatureChart data={notices} />
            </div>
            <div className={styles.title}>Шкала комфорта водички</div>
            <WaterComfortScale
              waterTemperature={notices.slice(-1).pop()?.termo as number}
            />
          </div>

          <div className={styles.watsenInfo}>
            <div
              className={classNames(
                styles.circle,
                'd-flex flex-column justify-content-center align-items-center',
              )}
            >
              <div className={styles.title}>Вода</div>
              <div className={classNames('d-block', styles.termo)}>
                {RoundValue(notices.slice(-1).pop()?.termo, 1)}
                <small>˚C</small>
              </div>
              <div className={styles.date}>
                {waterTempUpdateTime !== 0
                  ? `${waterTempUpdateTime} назад`
                  : 'только что'}
              </div>
              <div className={styles.watsenLogo}>
                <WatsenLogo />
              </div>
            </div>
          </div>
        </>
      ) : !loading && !notices ? (
        <div className="h-100 w-100 d-flex align-items-center justify-content-center fs-3">
          Нет данных для отображения :(
        </div>
      ) : (
        <Loader className={styles.loader} />
      )}
    </div>
  );
};

export default WaterTemperatureSection;
