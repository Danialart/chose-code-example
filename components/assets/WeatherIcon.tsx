import React, {FunctionComponent} from 'react';

const WeatherIcon: FunctionComponent<{className: string}> = ({className}) => {
  return (
    <svg
      className={className}
      width="137"
      height="157"
      viewBox="0 0 137 157"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_f_0:1)">
        <path
          d="M110.318 138.717C110.318 140.189 91.5956 141.383 68.5 141.383C45.4044 141.383 26.6816 140.189 26.6816 138.717C26.6816 137.244 45.4044 136.051 68.5 136.051C91.5956 136.051 110.318 137.244 110.318 138.717Z"
          fill="#BAC7CB"
        />
      </g>
      <path
        d="M136.768 31.8984C136.768 48.9783 122.846 62.8242 105.672 62.8242C88.4982 62.8242 74.5762 48.9783 74.5762 31.8984C74.5762 14.8186 88.4982 0.972656 105.672 0.972656C122.846 0.972656 136.768 14.8186 136.768 31.8984Z"
        fill="url(#paint0_linear_0:1)"
      />
      <path
        d="M136.768 76.2994C136.768 88.8817 126.493 99.082 113.813 99.082H30.7528C13.898 99.082 0.232422 85.5206 0.232422 68.7851C0.232422 52.0545 13.898 38.4882 30.7528 38.4882C32.2644 38.4882 33.7502 38.6034 35.207 38.813C42.0994 25.2259 56.2743 15.9023 72.6488 15.9023C92.4623 15.9023 109.061 29.5582 113.422 47.9046C113.857 49.7364 114.171 51.6129 114.351 53.5247V53.5295C126.776 53.8111 136.768 63.8962 136.768 76.2994Z"
        fill="#BAC7CB"
        fillOpacity="0.4"
      />
      <g filter="url(#filter1_b_0:1)">
        <path
          d="M136.768 76.2994C136.768 88.8817 126.493 99.082 113.813 99.082H30.7528C13.898 99.082 0.232422 85.5206 0.232422 68.7851C0.232422 52.0545 13.898 38.4882 30.7528 38.4882C32.2644 38.4882 33.7502 38.6034 35.207 38.813C42.0994 25.2259 56.2743 15.9023 72.6488 15.9023C92.4623 15.9023 109.061 29.5582 113.422 47.9046C113.857 49.7364 114.171 51.6129 114.351 53.5247V53.5295C126.776 53.8111 136.768 63.8962 136.768 76.2994Z"
          fill="url(#paint1_linear_0:1)"
        />
      </g>
      <g filter="url(#filter2_b_0:1)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M35.6136 39.5898L35.1047 39.5166C33.6793 39.3116 32.2278 39.1992 30.7528 39.1992C14.2916 39.1992 0.947266 52.4484 0.947266 68.7851C0.947266 85.1266 14.2915 98.3711 30.7528 98.3711H113.813C126.099 98.3711 136.053 88.4878 136.053 76.2994C136.053 64.2849 126.374 54.5131 114.335 54.2402L113.637 54.2244V53.5581C113.459 51.6912 113.151 49.8581 112.726 48.0682C108.44 30.036 92.1242 16.6133 72.6488 16.6133C56.554 16.6133 42.6206 25.7768 35.8452 39.1332L35.6136 39.5898ZM114.351 53.5247V53.5295C126.776 53.8111 136.768 63.8962 136.768 76.2994C136.768 88.8817 126.493 99.082 113.813 99.082H30.7528C13.898 99.082 0.232422 85.5206 0.232422 68.7851C0.232422 52.0545 13.898 38.4882 30.7528 38.4882C32.2644 38.4882 33.7502 38.6034 35.207 38.813C42.0994 25.2259 56.2743 15.9023 72.6488 15.9023C92.4623 15.9023 109.061 29.5582 113.422 47.9046C113.857 49.7364 114.171 51.6129 114.351 53.5247Z"
          fill="url(#paint2_linear_0:1)"
        />
      </g>
      <path
        d="M136.768 76.2994C136.768 88.8817 126.493 99.082 113.813 99.082H30.7528C13.898 99.082 0.232422 85.5206 0.232422 68.7851C0.232422 52.0545 13.898 38.4882 30.7528 38.4882C32.2644 38.4882 33.7502 38.6034 35.207 38.813C42.0994 25.2259 56.2743 15.9023 72.6488 15.9023C92.4623 15.9023 109.061 29.5582 113.422 47.9046C113.857 49.7364 114.171 51.6129 114.351 53.5247V53.5295C126.776 53.8111 136.768 63.8962 136.768 76.2994Z"
        fill="url(#pattern0)"
      />
      <path
        d="M68.6303 105.125C64.0667 107.753 60.9941 112.657 60.9941 118.278C60.9941 118.479 60.9969 118.679 61.0053 118.877C61.3787 128.425 75.8818 128.425 76.2552 118.877C76.2636 118.679 76.2664 118.479 76.2664 118.278C76.2678 112.657 73.1938 107.753 68.6303 105.125Z"
        fill="#00A3FF"
      />
      <path
        d="M39.3231 84.1523C34.7595 86.7799 31.6855 91.6844 31.6855 97.3054C31.6855 97.5063 31.6897 97.7059 31.6981 97.9054C32.0701 107.452 46.5746 107.452 46.9481 97.9054C46.955 97.7059 46.9592 97.5063 46.9592 97.3054C46.9592 91.6844 43.8866 86.7799 39.3231 84.1523Z"
        fill="#00A3FF"
      />
      <path
        d="M101.981 90.9062C97.4173 93.5338 94.3447 98.4383 94.3447 104.059C94.3447 104.26 94.3489 104.46 94.3559 104.659C94.7293 114.206 109.232 114.206 109.606 104.659C109.614 104.46 109.617 104.26 109.617 104.059C109.618 98.4383 106.546 93.5338 101.981 90.9062Z"
        fill="#00A3FF"
      />
      <defs>
        <filter
          id="filter0_f_0:1"
          x="11.7835"
          y="121.153"
          width="113.433"
          height="35.1283"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          />
          <feGaussianBlur
            stdDeviation="7.44908"
            result="effect1_foregroundBlur_0:1"
          />
        </filter>
        <filter
          id="filter1_b_0:1"
          x="-30.8054"
          y="-15.1355"
          width="198.611"
          height="145.255"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feGaussianBlur in="BackgroundImage" stdDeviation="15.5189" />
          <feComposite
            in2="SourceAlpha"
            operator="in"
            result="effect1_backgroundBlur_0:1"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_backgroundBlur_0:1"
            result="shape"
          />
        </filter>
        <filter
          id="filter2_b_0:1"
          x="-30.8054"
          y="-15.1355"
          width="198.611"
          height="145.255"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feGaussianBlur in="BackgroundImage" stdDeviation="15.5189" />
          <feComposite
            in2="SourceAlpha"
            operator="in"
            result="effect1_backgroundBlur_0:1"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_backgroundBlur_0:1"
            result="shape"
          />
        </filter>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width="1.00023"
          height="1.64182"
        >
          <use transform="scale(0.0045465 0.00746284)" />
        </pattern>
        <linearGradient
          id="paint0_linear_0:1"
          x1="118.198"
          y1="3.83585"
          x2="92.2393"
          y2="62.9227"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#FFD88B" />
          <stop offset="1" stopColor="#FFA900" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_0:1"
          x1="21.8564"
          y1="30.4766"
          x2="118.36"
          y2="99.5817"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0.416176" stopColor="white" stopOpacity="0.7" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_0:1"
          x1="-10.223"
          y1="37.8213"
          x2="38.4796"
          y2="73.0436"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <image id="image0_0:1" width="220" height="220" />
      </defs>
    </svg>
  );
};

export default WeatherIcon;
