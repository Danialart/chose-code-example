import React, {FunctionComponent} from 'react';

import Image from 'next/image';

import {formatImagePath} from '../helpers/helpers';
import styles from './../styles/Modules/Header.module.scss';
import classNames from 'classnames';
import WeatherIcon from './assets/WeatherIcon';
import {TvEntity, TvModulesType} from '../helpers/types/TvTypes';

const Header: FunctionComponent<{
  minimal?: boolean;
  activeModule: TvModulesType;
  tv: TvEntity;
}> = ({tv, activeModule, minimal = false}) => {
  return (
    <header
      className={classNames(
        styles.header,
        minimal
          ? 'd-flex justify-content-center align-items-center minimal'
          : '',
      )}
    >
      <div className={styles.logo}>
        <Image
          src={formatImagePath(tv.design_options?.logo as string)}
          height="103.2px"
          width="385px"
          alt={tv.design_options?.title}
          objectFit="contain"
          objectPosition={0}
          layout="intrinsic"
        />
      </div>
      {!minimal ? (
        <div
          className={classNames(
            'ms-auto d-flex align-items-center',
            styles.rightDescription,
          )}
        >
          <div className={styles.info}>
            <strong className={classNames('d-block', styles.title)}>
              Погода
            </strong>
            {activeModule === 'weather_min' ? (
              <>Фактические данные</>
            ) : activeModule == 'weather' ? (
              <>Прогноз погоды</>
            ) : activeModule === 'water_temperature' ? (
              <>Температура воды в море</>
            ) : null}
          </div>
          <WeatherIcon className={styles.icon} />
        </div>
      ) : null}
    </header>
  );
};
export default Header;
