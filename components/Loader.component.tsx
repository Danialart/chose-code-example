import classNames from 'classnames';
import React, {FunctionComponent, PropsWithChildren} from 'react';
import styles from '../styles/Modules/Loader.module.scss';

const Loader: FunctionComponent<PropsWithChildren<{className?: string}>> = ({
  className,
}) => {
  return (
    <div className={classNames(className, styles.loaderContainer)}>
      <div className={styles.spinner}></div>
    </div>
  );
};

export default Loader;
