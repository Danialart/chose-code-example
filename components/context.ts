import React from 'react';
import {BeachPoint} from '../helpers/types/BeachPoint.interface';

import {TvEntity, TvModulesType} from '../helpers/types/TvTypes';

type TvContextType = {
  tv: TvEntity;
  beachData?: BeachPoint;
  activeModule: TvModulesType;
  toggleSlideChanging: (state: boolean) => void;
  changeAciveModule: () => void;
  slideChangeIsEnabled: boolean;
};

export const TvContext: React.Context<TvContextType> = React.createContext({
  tv: {} as TvEntity,
} as TvContextType);
