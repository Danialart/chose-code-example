import React, {FunctionComponent, PropsWithChildren} from 'react';
import Head from 'next/head';

const MainLayout: FunctionComponent<PropsWithChildren<{title: string}>> = ({
  title,
  children,
}) => {
  return (
    <>
      <Head>
        <title>Приставка {title}</title>
      </Head>
      <div className="wrapper">{children}</div>
    </>
  );
};

export default MainLayout;
