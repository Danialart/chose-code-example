import {Gradation} from './types/BeachPoint.interface';
import {TvEntity, TvModulesType} from './types/TvTypes';
import moment from 'moment-timezone';

export const formatImagePath = (path: string) =>
  `${process.env.BACKEND_URL_PRIVATE}/${path}`;

/**
 * Get module rotation time in miliseconds
 * @param tv Tv entity
 * @param activeModule string, name of active module
 * @param defaultTime number, time in seconds
 * @returns number, time in miliseconds
 */
export const getModuleRotationTime = (
  tv: TvEntity,
  activeModule: TvModulesType,
  defaultTime: number,
): number => {
  let interval: number = secondsToMilliseconds(defaultTime);

  if (
    activeModule === 'weather_min' &&
    tv.weather_min_settings?.rotation_block_time
  )
    interval = secondsToMilliseconds(
      tv.weather_min_settings.rotation_block_time,
    );
  if (activeModule === 'weather' && tv.weather_settings?.rotation_block_time)
    interval = secondsToMilliseconds(tv.weather_settings.rotation_block_time);
  if (
    activeModule === 'water_temperature' &&
    tv.water_temp_settings?.rotation_block_time
  )
    interval = secondsToMilliseconds(
      tv.water_temp_settings.rotation_block_time,
    );
  if (
    activeModule === 'advertisement' &&
    tv.advertisement_settings?.rotation_block_time
  )
    interval = secondsToMilliseconds(
      tv.advertisement_settings.rotation_block_time,
    );

  return interval;
};

export const secondsToMilliseconds = (secondsTime: number) => {
  return secondsTime * 1000;
};

export const RoundValue = (value?: number, decimals?: number): number => {
  return !value
    ? 0
    : parseFloat((Math.round(value * 100) / 100).toFixed(decimals));
};

export const modulesList: TvModulesType[] = [
  'weather_min',
  'weather',
  'water_temperature',
  'advertisement',
];

export const calculateWindDirection = (deg: number) => {
  if (deg > 0 && deg <= 45) return 'С/В';
  if (deg > 45 && deg <= 90) return 'В';
  if (deg > 90 && deg <= 135) return 'Ю/В';
  if (deg > 135 && deg <= 180) return 'Ю';
  if (deg > 180 && deg <= 225) return 'Ю/З';
  if (deg > 225 && deg <= 270) return 'З';
  if (deg > 270 && deg <= 315) return 'С/З';
  if (deg > 315 && deg <= 360) return 'С';
  return 'штиль';
};

export const getStormData = (
  stormGradations: Gradation[],
  windSpeed: number,
  windDegrees: number,
) => {
  let value = 'штиль';

  stormGradations.map((gradation) => {
    if (
      windSpeed >= gradation.wind_speed_start &&
      (windSpeed <= gradation.wind_speed_end || gradation.wind_speed_end === 0)
    ) {
      // Wind matches, check degrees
      if (
        windDegrees >= gradation.wind_degree_start &&
        windDegrees <= gradation.wind_degree_end
      )
        value = gradation.value;
      else if (windSpeed > 3) return 'волны';
    }
  });

  return value;
};

export const momentWithMoscowTz = (time?: moment.MomentInput) => {
  return moment(time).tz('Europe/Moscow');
};
