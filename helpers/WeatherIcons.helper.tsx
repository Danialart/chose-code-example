import React from 'react';

const BaseIconPath = `${process.env.NEXT_PUBLIC_BASE_PATH}/weather-icons`;

const Icons = [
  {
    code: ['01d'],
    icon: `${BaseIconPath}/sunny.png`,
  },
  {
    code: ['01n'],
    icon: `${BaseIconPath}/sunny-night.png`,
  },
  {
    code: ['02d'],
    icon: `${BaseIconPath}/cloudy-day.png`,
  },
  {
    code: ['02n'],
    icon: `${BaseIconPath}/cloudy-night.png`,
  },
  {
    code: ['03d', '03n'],
    icon: `${BaseIconPath}/clouds.png`,
  },
  {
    code: ['04d', '04n'],
    icon: `${BaseIconPath}/broken-clouds.png`,
  },
  {
    code: ['09d', '09n'],
    icon: `${BaseIconPath}/shower-rain.png`,
  },
  {
    code: ['10d'],
    icon: `${BaseIconPath}/rain-day.png`,
  },
  {
    code: ['10n'],
    icon: `${BaseIconPath}/rain-night.png`,
  },
  {
    code: ['11n', '11d'],
    icon: `${BaseIconPath}/thunderstrom.png`,
  },
  {
    code: ['13n', '13d'],
    icon: `${BaseIconPath}/snow.png`,
  },
  {
    code: ['50n', '50d'],
    icon: `${BaseIconPath}/mist.png`,
  },
];

export const getWeatherIcon = (code: string) => {
  const itemExist = Icons.find((item) =>
    item.code.some((foundCode) => foundCode === code),
  );
  return itemExist?.icon || null;
};
