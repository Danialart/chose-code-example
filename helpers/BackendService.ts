import Axios, {Method} from 'axios';
import {TvEntity} from './types/TvTypes';
import * as Sentry from '@sentry/nextjs';
import {WeatherType, WeatherMin} from './types/WeatherType';
import {BeachPoint} from './types/BeachPoint.interface';
import {Notice} from './types/NoticesTypes';

export default class BackendService {
  private request<T>(
    method: Method,
    actionUrl: string,
    requestData?: {
      [x: string]: any;
    },
  ): Promise<T> {
    return new Promise<T>((resolve, reject) =>
      Axios.request({
        method,
        url: `${
          process.browser
            ? process.env.NEXT_PUBLIC_BACKEND_URL
            : process.env.BACKEND_URL_PRIVATE
        }${actionUrl}`,
        data: requestData,
        params:
          method === 'get' || (method === 'GET' && requestData)
            ? requestData
            : undefined,
      })
        .then((res) => res.data)
        .then((data) => resolve(data))
        .catch((error) => {
          console.error('Request error', error);
          reject(error);
        }),
    );
  }

  getTvById(id: string) {
    return this.request<TvEntity>('get', `/tv/get/${id}`);
  }

  async getWeatherMinData(
    stationId: number,
    limit: number,
  ): Promise<WeatherMin> {
    const data = await this.request<{data: WeatherMin[]}>(
      'get',
      '/api/getWeatherFromMeteoStation',
      {
        stationId,
        offset: 0,
        limit: limit,
      },
    );

    return data.data[0];
  }

  getBeachData(beach_id: number): Promise<BeachPoint> {
    return this.request('get', '/api/getBeachInformation', {
      beach_id,
    });
  }

  getWeatherData(beachId: number) {
    return this.request<WeatherType[]>('get', `/api/getWeatherForBeachPoint`, {
      beachId,
    });
  }

  getSensorData(sensorId: number) {
    return this.request<Notice[]>('get', '/api/getSensorNotices', {sensorId});
  }

  handleError(error: unknown) {
    // Implement sentry
    console.error(error);
    Sentry.captureException(error);
  }
}
