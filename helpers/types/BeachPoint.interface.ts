import {Notice} from './NoticesTypes';

export type Gradation = {
  value: 'Штиль' | 'Волны' | 'Шторм';
  wind_speed_start: number;
  wind_speed_end: number;
  wind_degree_start: number;
  wind_degree_end: number;
};

export interface BeachPoint {
  id: number;
  title: string;
  sensor_number: number;
  sputnik_number: number;
  lastNotice?: Notice;
  data_radius?: number;
  lat: number | null;
  lng: number | null;
  enabled: boolean;
  is_checkpoint: boolean;
  location: string;
  weather_lat: number | null;
  weather_lng: number | null;
  data_relevance: number;
  point_relevance: number;
  yandex_weather_link?: string;
  yandex_map_link?: string;
  gradations: Gradation[];
  beach_type_1: boolean;
  beach_type_2: boolean;
  beach_type_3: boolean;
  beach_type_4: boolean;
  beach_type_5: boolean;
  toilet: boolean;
  changeroom: boolean;
  shower: boolean;
  taxi_boat: boolean;
  boat_schedule: string;
  bar: boolean;
  bar_vk?: string;
  bar_insta?: string;
  bar_website?: string;
  bar_phone?: string;
  boat_services: boolean;
  boat_services_name: string;
  boat_services_phone: string;
}
