export interface Notice {
  created: number;
  id: number;
  lat: null | number;
  lng: null | number;
  sig: number | null;
  termo: null | number;
  termo1: null | number;
  termo2: null | number;
  termo3: null | number;
  vcc: number | null;
  waves: number | null;
}
