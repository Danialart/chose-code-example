export interface WeatherType {
  id: number;
  lat: number;
  lng: number;
  date: number;
  temp: number;
  feels_like: number;
  pressure: number;
  humidity: number;
  wind_speed: number;
  wind_deg: number;
  weather_icon: string;
  pop: number;
  created: number;
}

export interface WeatherMin {
  date: number;
  UV: number;
  PR: number;
  HM: number;
  UVI: number;
  L: number;
  WD: number;
  WV2: number;
  PR1: number;
  td: number;
  RSSI: number;
  t: number;
  WM: number;
  RN: number;
  LI: number;
  WV: number;
  id: number;
  created: string;
  updated: string;
}
