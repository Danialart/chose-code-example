export type Notice = {
  sensor: string;
  termo: number;
  termo1?: number;
  termo2?: number;
  termo3?: number;
  waves?: number;
  vcc?: number;
  sig?: number;
  lat?: number;
  lng?: number;
  created: number;
};
