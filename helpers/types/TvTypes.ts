import {AdvertiseSettings, AdvertiseType} from './AdvertiseTypes';

export interface TvDesign {
  id: string;
  title: string;
  logo: string | File;
}

export interface TvEntity {
  id: string;
  title: string;
  address: null | string;
  enabled: boolean | string;
  activated_until: number;
  weather_lat: number;
  weather_lng: number;
  comment: null | string;
  meteo_station_id: number | null;
  beach_id: number | null;
  design_options?: TvDesign;
  modules_enabled?: TvModuleEnabled[];
  advertisements?: AdvertiseType[];
  advertisement_settings?: AdvertiseSettings;
  weather_min_settings?: ModuleSettings;
  weather_settings?: ModuleSettings;
  water_temp_settings?: ModuleSettings;
}
export type ModuleSettings = {
  id: number;
  rotation_block_time: number | null;
};

export type TvModuleEnabled = {
  id?: number;
  module_code: TvModulesType;
};

export type TvModulesType =
  | 'weather_min'
  | 'weather'
  | 'water_temperature'
  | 'advertisement';
