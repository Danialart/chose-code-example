import {fullpageApi} from '@fullpage/react-fullpage';

export type ValueOf<T> = T[keyof T];

export type ReactFullPageApi = {
  fullpageApi: fullpageApi;
};
