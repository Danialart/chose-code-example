export type FormDataType = {
  email: string;
  password: string;
};

export type ValidationErrorType = {
  email?: boolean | undefined;
  password?: boolean | undefined;
};

export type UserInfoType = {
  userInfo?: {
    email: string;
    name: string;
    id: number;
    role: string;
  };
};

export type AuthCookiesType = {
  token: string;
  refresh_token: string;
};
