import {TvEntity} from './TvTypes';
export interface AdvertiseType {
  id: number;
  type: 'video' | 'photo';
  title: string;
  enabled: boolean;
  url: string;
  link: string;
  tv: TvEntity;
}

export interface AdvertiseSettings {
  id: number;

  rotation_time: number | null;

  rotation_block_time: number | null;
}
